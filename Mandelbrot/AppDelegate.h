//
//  AppDelegate.h
//  Mandelbrot
//
//  Created by Philip Huffman on 2015-06-25.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

