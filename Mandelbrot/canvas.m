//
//  canvas.m
//  Mandelbrot
//
//  Created by Philip Huffman on 2015-06-25.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

/*
 The simplest algorithm for generating a representation of the Mandelbrot set is known as the
 "escape time" algorithm. A repeating calculation is performed for each x, y point in the plot
 area and based on the behavior of that calculation, a color is chosen for that pixel.
 
 The x and y locations of each point are used as starting values in a repeating, or iterating
 calculation. The result of each iteration is used as the starting values for the next. The
 values are checked during each iteration to see if they have reached a critical 'escape'
 condition or 'bailout'. If that condition is reached, the calculation is stopped, the pixel
 is drawn, and the next x, y point is examined. For some starting values, escape occurs quickly,
 after only a small number of iterations. For starting values very close to but not in the set,
 it may take hundreds or thousands of iterations to escape. For values within the Mandelbrot set,
 escape will never occur. The programmer or user must choose how much iteration, or 'depth,' they
 wish to examine. The higher the maximum number of iterations, the more detail and subtlety emerge
 in the final image, but the longer time it will take to calculate the fractal image.
 
 Escape conditions can be simple or complex. Because no complex number with a real or imaginary
 part greater than 2 can be part of the set, a common bailout is to escape when either coefficient
 exceeds 2. A more computationally complex method that detects escapes sooner, is to compute distance
 from the origin using the Pythagorean theorem, i.e., to determine the absolute value, or modulus,
 of the complex number. If this value exceeds two, the point has reached escape. More computationally
 intensive rendering variations include the Buddhabrot method, which finds escaping points and plots
 their iterated coordinates.
 
 The color of each point represents how quickly the values reached the escape point. Often black is
 used to show values that fail to escape before the iteration limit, and gradually brighter colors are
 used for points that escape. This gives a visual representation of how many cycles were required before
 reaching the escape condition.
 
 To render such an image, the region of the complex plane we are considering is subdivided into a certain
 number of pixels. To color any such pixel, let c be the midpoint of that pixel. We now iterate the
 critical point 0 under P_c, checking at each step whether the orbit point has modulus larger than 2. When
 this is the case, we know that c does not belong to the Mandelbrot set, and we color our pixel according to
 the number of iterations used to find out. Otherwise, we keep iterating up to a fixed number of steps, after
 which we decide that our parameter is "probably" in the Mandelbrot set, or at least very close to it, and
 color the pixel black.
 
 The algorithm does not use complex numbers, and manually simulates complex number operations using a CGPoint.
 
 For each pixel (Px, Py) on the screen, do:
 {
     x0 = scaled x coordinate of pixel (scaled to lie in the Mandelbrot X scale (-2.5, 1))
     y0 = scaled y coordinate of pixel (scaled to lie in the Mandelbrot Y scale (-1, 1))
     x = 0.0
     y = 0.0
     iteration = 0
     max_iteration = 1000
     while ( x*x + y*y < 2*2  AND  iteration < max_iteration )
     {
         xtemp = x*x - y*y + x0
         y = 2*x*y + y0
         x = xtemp
         iteration = iteration + 1
     }
     color = palette[iteration]
     plot(Px, Py, color)
 }
 */

#import "canvas.h"

static const int N_MAX = 64;
static const double SCALE_FACTOR = 2.0;
static const double RECT_SIZE = 1.0;

@implementation canvas

- (void)drawRect:(CGRect)rect {
    int n = 0;
    
    CGPoint currentPoint, positionOnScreen, z;
    
    for (positionOnScreen.y = rect.origin.y; positionOnScreen.y < rect.size.height; positionOnScreen.y += RECT_SIZE) {
        for (positionOnScreen.x = rect.origin.x; positionOnScreen.x < rect.size.width; positionOnScreen.x += RECT_SIZE) {
            currentPoint = CGPointMake(SCALE_FACTOR * (3.5 * positionOnScreen.x / rect.size.width - 2.5),
                                       SCALE_FACTOR * (2.0 * positionOnScreen.y / rect.size.height - 1.0));
            z = CGPointZero;
            
            for (n = 0; n < N_MAX && [self cSqr:z] < 4.0; n++) {
                z = [self mandelbrot:z
                                    :currentPoint];
            }
            
            if (n >= N_MAX) {
                [[UIColor blackColor] set];
            } else if (n > N_MAX >> 2) {
                [[UIColor redColor] set];
            } else if (n > N_MAX >> 3) {
                [[UIColor yellowColor] set];
            } else {
                [[UIColor colorWithRed:[self setColorRange:n :N_MAX] * 0.25
                                 green:[self setColorRange:n :N_MAX] * 0.5
                                  blue:[self setColorRange:n :N_MAX]
                                 alpha:1.0] set];

            }

            CGMutablePathRef path = CGPathCreateMutable();
            CGPathAddRect(path, nil, CGRectMake(positionOnScreen.x, positionOnScreen.y, RECT_SIZE, RECT_SIZE));
            CGContextRef graphicsContext = UIGraphicsGetCurrentContext();
            CGContextAddPath(graphicsContext, path);
            CGContextDrawPath(graphicsContext, kCGPathStroke);
            CGPathRelease(path);
        }
    }
}

-(double) cSqr :(CGPoint)z {
    return z.x * z.x + z.y * z.y;
}

-(double) setColorRange :(int)value
                        :(int)valueMax {
    return 1.0 - value / (double)valueMax;
}

-(CGPoint) mandelbrot :(CGPoint)p
                      :(CGPoint)c {
    return CGPointMake(p.x * p.x - p.y * p.y + c.x,
                       p.x * p.y * 2.0 + c.y);
    
}

-(CGPoint) cp :(CGPoint)p
              :(CGPoint)c {
    return CGPointMake((p.y <= 0.0 ? p.y * p.y - M_PI * p.y + M_PI / 2.0 : M_PI * p.x + p.x * p.x + 7.0) + c.x,
                       p.x * p.y * 2.0 + c.y);
    
}

@end
